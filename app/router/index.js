import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import {
  Login,
  User,
  Maps, 
  Dashboaard, 
  Aplikasi, 
  AplikasiAdd, 
  AplikasiEdit, 
  ListAplikasi, 
  PenggunaAplikasi, 
  PenggunaAdd, 
  PenggunaEdit, 
  ListAplikasiNotifikasi,
  NotifikasiAplikasi,
  NotifikasiAdd,
  NotifikasiKirim
} from '../src/views';


const Stack = createStackNavigator();

const Router = () => {
    return (
      <Stack.Navigator initialRouteName="Login" screenOptions={{header:()=>null}}>
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="User"
          component={User}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Maps"
          component={Maps}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Dashboard"
          component={Dashboaard}
          options={{headerShown: false}}
        />
        {/* Kategori */}
          <Stack.Screen
            name="Aplikasi"
            component={Aplikasi}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="AplikasiAdd"
            component={AplikasiAdd}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="AplikasiEdit"
            component={AplikasiEdit}
            options={{headerShown: false}}
          />
        {/* -- */}

        <Stack.Screen
            name="ListAplikasi"
            component={ListAplikasi}
            options={{headerShown: false}}
          />

        <Stack.Screen
            name="PenggunaAplikasi"
            component={PenggunaAplikasi}
            options={{headerShown: false}}
          />

        <Stack.Screen
            name="PenggunaAdd"
            component={PenggunaAdd}
            options={{headerShown: false}}
          />
        <Stack.Screen
            name="PenggunaEdit"
            component={PenggunaEdit}
            options={{headerShown: false}}
          />


        <Stack.Screen
            name="ListAplikasiNotifikasi"
            component={ListAplikasiNotifikasi}
            options={{headerShown: false}}
          />

        <Stack.Screen
            name="NotifikasiAplikasi"
            component={NotifikasiAplikasi}
            options={{headerShown: false}}
          />

        <Stack.Screen
            name="NotifikasiAdd"
            component={NotifikasiAdd}
            options={{headerShown: false}}
          />

        <Stack.Screen
            name="NotifikasiKirim"
            component={NotifikasiKirim}
            options={{headerShown: false}}
          />
      </Stack.Navigator>
    );
  };
  
  export default Router;