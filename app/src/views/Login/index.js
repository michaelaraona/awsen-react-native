import React,{
    useState
} from 'react';
import {
    View,
    Text , 
    TextInput,
    TouchableOpacity,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import  Icon  from 'react-native-vector-icons/MaterialIcons';
import COLORS from '../../consts/color';
import STYLES from '../../styles';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import {ipGlobal} from './../../Global';
import AsyncStorage from "@react-native-async-storage/async-storage";

const Login = ({navigation}) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    
    const onPress = () => {
        // navigation.navigate('Maps');
        if(username == '' || password == ''){
            Toast.show("Lengkapi Email & Password !");
        }else{
            const login = async () => {
                try{
                    const formData = new FormData();
                    formData.append('username',username);
                    formData.append('password',password);
                    const response = await axios.post(`http://${ipGlobal}/ManajemenNotifikasi/AuthApi/authUser`,formData);

                    const user = JSON.stringify(response.data.users);
                    await AsyncStorage.setItem("user", user);
                    
                    if(response.data.status == 200){
                        Toast.showWithGravity("Selamat Datang " + response.data.users.nama_lengkap, Toast.LONG, Toast.TOP);
                        navigation.navigate('Dashboard');
                    }else{
                        Toast.showWithGravity(response.data.message, Toast.LONG, Toast.TOP);
                    }
                }catch(error){
                    console.log(error.message);
                }
            };
            
            login();
        }
    };
    
    return (
        <SafeAreaView style={{paddingHorizontal:20,flex:1,backgroundColor:COLORS.white}}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{flexDirection:'row',marginTop:100}}>
                    <Text style={{fontWeight: "bold", fontSize:22,color:COLORS.dark}}>Aw</Text>
                    <Text style={{fontWeight: "bold", fontSize:22,color:COLORS.secondary}}>sen</Text>
                </View>

                <View style={{marginTop: 70}}>
                    <Text style={{fontSize:27, fontWeight: "bold",color: COLORS.dark}}>Selamat Datang,</Text>
                    <Text style={{fontSize:17, fontWeight: "bold",color: COLORS.light}}>Silahkan Login terlebih dahulu </Text>
                </View>

                <View style={{marginTop:20}}>
                    <View style={STYLES.inputContainer}>
                        <Icon name="person-outline" size={20} color={COLORS.light} style={STYLES.inputIcon}/>
                        <TextInput 
                            placeholder="Username" 
                            style={STYLES.input}
                            value={username}
                            onChangeText={value => setUsername(value)}
                        />
                    </View>

                    <View style={STYLES.inputContainer}>
                        <Icon name="lock-outline" size={20} color={COLORS.light} style={STYLES.inputIcon}/>
                        <TextInput 
                            placeholder="Password" 
                            style={STYLES.input}
                            secureTextEntry
                            value={password}
                            onChangeText={value => setPassword(value)}
                        />
                    </View>

                    
                    <TouchableOpacity
                            onPress={onPress}>
                        <View style={STYLES.btnPrimary}>
                            <Text style={{color: COLORS.white, fontWeight: 'bold', fontSize:18}}>Sign in</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

export default Login;