import * as React  from 'react';
import {
    View,
    Text , 
    TextInput,
    Button,
    TouchableOpacity,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import COLORS from '../../consts/color';
import STYLES from '../../styles';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import { ipGlobal } from '../../Global';
import MultiSelect from 'react-native-multiple-select';

const NotifikasiKirim = ({route,navigation}) => {
    
    const [notifikasi,setNotifikasi] = React.useState([]);
    const {id,aplikasi_id,nama_aplikasi} = route.params;
    const [dataUser , setDataUser] = React.useState([]);

    
    React.useEffect(() =>{
        const getData = async function() {
            try{
                const response = await axios.get(`http://${ipGlobal}/ManajemenNotifikasi/NotifikasiApi/getNotifikasiByid/${id}`);
                setNotifikasi(response.data.notifikasi);
            }catch(error){
                console.log(error);
            }
        };

        const timer = setTimeout(() => {
            getData();
            }, 5000);
        

        
        return () => clearTimeout(timer);
    }, [notifikasi]);

    // console.log(notifikasi);
    React.useEffect(function() {
        (async function() {
          const userJson = await AsyncStorageLib.getItem("user");
          const user = JSON.parse(userJson);
    
          setDataUser(user);
        })();
      }, []);
      
    
    const kirim = () => {
        const a = async () => {
            try{
                
                const response = await axios.get(`http://${ipGlobal}/ManajemenNotifikasi/NotifikasiApi/kirimNotifikasi/${aplikasi_id}/${id}`);
                if(response.data.status == 200){
                    Toast.showWithGravity(response.data.message , Toast.LONG, Toast.TOP);
                    navigation.navigate('NotifikasiAplikasi',{
                        id:aplikasi_id,
                        nama_aplikasi:nama_aplikasi,
                    });
                }else{
                    Toast.showWithGravity(response.data.message, Toast.LONG, Toast.TOP);
                }
                console.log(response.data);
            }catch(error){
                console.log(error.message);
            }
        };
        
        a();
    };

    // console.log(notifikasi);

    let row;
    if(notifikasi.status == 1){
        console.log('sini');
        row = (
            <View style={{ marginTop:100 }}>
                            
                <Button
                color={COLORS.back}
                title="Kembali"
                onPress={ function () {
                    navigation.navigate('NotifikasiAplikasi',{
                        id:aplikasi_id,
                        nama_aplikasi:nama_aplikasi,
                    });
                }}
                
                />
                <Text style={{ fontSize:18, fontWeight: "bold",color: COLORS.dark,marginBottom:5, marginTop:20 }}>Sudah Terkirim</Text>

            </View>
        );
    }else{
       row = ( <View style={{ marginTop:100 }}>
                            
                <Button
                color={COLORS.back}
                title="Kembali"
                onPress={ function () {
                    navigation.navigate('NotifikasiAplikasi',{
                        id:aplikasi_id,
                        nama_aplikasi:nama_aplikasi,
                    });
                }}
                
                />

                <Button
                color={COLORS.success}
                title="Kirim"
                onPress={  () => {
                    kirim();
                }}
                
                />

            </View>)
    }
    
    return (
        <SafeAreaView style={{paddingHorizontal:20,flex:1,backgroundColor:COLORS.white}}>
            
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ marginBottom:100 }}>
                    <View>
                        {row}
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

export default NotifikasiKirim;