import * as React from 'react';
import {
    View,
    Text , 
    TextInput,
    Button,
    TouchableOpacity,
} from 'react-native';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import { SafeAreaView } from 'react-native-safe-area-context';
import COLORS from '../../consts/color';
import STYLES from '../../styles';
import { ipGlobal } from '../../Global';

const renderItem = function({item},navigation,nama_aplikasi) {
    let status ;
    if(item.status == 1){
        status = (<Text style={{ fontSize: 13,color:'black' }}>Status : Sudah Terkirim</Text>);
    }else{
        status = (<Text style={{ fontSize: 13,color:'black' }}>Status : Belum Terkirim</Text>);
    }
    return(
        <TouchableOpacity
            onPress={ function() {
                navigation.navigate("NotifikasiKirim", {
                    id: item.id_notifikasi,
                    aplikasi_id : item.aplikasi_id,
                    nama_aplikasi:item.nama_aplikasi,
                });
                
            }}
        >
            <View style={{ flexDirection: "row", flexWrap: "wrap"  ,marginBottom:10}}>
                <View style={{ marginBottom: 5 }}>
                    <Text style={{ fontSize: 13,color:'black' }}>Tanggal Dibuat: {item.tanggalDibuat}</Text>
                    <Text style={{ fontSize: 13,color:'black' }}>Judul : {item.judul}</Text>
                    {status}
                    
                </View>
            </View>

        </TouchableOpacity>
    );
};

const NotifikasiAplikasi = ({route,navigation}) => {
    const [notifikasi,setNotifikasi] = React.useState([]);
    const {id,nama_aplikasi} = route.params;

    React.useEffect(() =>{
        const getData = async function() {
            try{
                const response = await axios.get(`http://${ipGlobal}/ManajemenNotifikasi/NotifikasiApi/getNotifikasi/${id}`);
                setNotifikasi(response.data.notifikasi);
            }catch(error){
                console.log(error);
            }
        };

        const timer = setTimeout(() => {
            getData();
            }, 5000);
        

        
        return () => clearTimeout(timer);
    }, [notifikasi]);

    // console.log(notifikasi);

    return (
        <View style={{ marginBottom: 500}}>
            
            <View style={{flexDirection: "row", justifyContent: "space-evenly",marginTop: 20, marginLeft:10}}>
                <Text style={{fontSize:20, fontWeight: "bold",color: COLORS.dark}}>Data Notifikasi Aplikasi ({nama_aplikasi})</Text>
            </View>
            <Text style={{ marginVertical: 15, fontSize: 16, fontWeight: "bold", textAlign: "center" }}>Klik Data Untuk Opsi Update & Delete</Text>
            <View style={{ flexDirection: "row", justifyContent: "space-evenly" }}>
                <Button
                color={COLORS.success}
                title="Tambah Data"
                onPress={()=> navigation.navigate('NotifikasiAdd',{
                    id:id,
                    nama_aplikasi:nama_aplikasi,
                })}
                
                />
                <Button
                color={COLORS.back}
                title="Kembali"
                onPress={()=> navigation.navigate('ListAplikasiNotifikasi')}
                
                />
            </View>
            <View style={{ borderBottomWidth: 3, borderBottomColor: "black", marginTop:10,marginBottom:20 }} />
            <View style={{ flexDirection: "row"}}>
            <FlatList
                style={{ backgroundColor: "white"}}
                data={notifikasi}
                renderItem={ item => renderItem(item, navigation,nama_aplikasi) }
                keyExtractor={item => item.id_notifikasi}
                ItemSeparatorComponent={function() {
                return <View style={{ borderBottomWidth: 1, borderBottomColor: "black",marginBottom:5 }} />;
                }}
            /> 
            </View>
           
        </View>
    );

};


export default NotifikasiAplikasi;
