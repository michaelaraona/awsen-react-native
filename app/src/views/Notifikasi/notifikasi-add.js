import * as React  from 'react';
import {
    View,
    Text , 
    TextInput,
    Button,
    TouchableOpacity,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import COLORS from '../../consts/color';
import STYLES from '../../styles';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import { ipGlobal } from '../../Global';
import MultiSelect from 'react-native-multiple-select';

const NotifikasiAdd = ({route,navigation}) => {
    const [judul, setJudul] = React.useState('');
    const [isi,setIsi] = React.useState('');
    const [pengguna,setPengguna] = React.useState([]);
    const [selectedItems,setselectedItems] = React.useState([]);
    const [media,setMedia] = React.useState([]);
    const [datamedia,setDataMedia] = React.useState([]);
    const {id,nama_aplikasi} = route.params;
    const [dataUser , setDataUser] = React.useState([]);

    React.useEffect(function() {
        (async function() {
          const userJson = await AsyncStorageLib.getItem("user");
          const user = JSON.parse(userJson);
    
          setDataUser(user);
        })();
      }, []);
    
    React.useEffect(function() {
        (async function() {
            
            const response = await axios.get(`http://${ipGlobal}/ManajemenNotifikasi/NotifikasiApi/getData/2`);
            setPengguna(response.data.pengguna);
            setDataMedia(response.data.media);
        
        })();
      }, []);
    
    // pengguna.map((row,index) => {
    //     return(

    //     );
    // });
    
    const tambahData = () => {
        const add = async () => {
            try{
                
                const formData = new FormData();
                formData.append('judul',judul);
                formData.append('isi',isi);
                formData.append('media_id',JSON.stringify(media));
                formData.append('penerima',JSON.stringify(selectedItems));
                const response = await axios.post(`http://${ipGlobal}/ManajemenNotifikasi/NotifikasiApi/tambahNotifikasi/${id}`,formData);
                if(response.data.status == 200){
                    Toast.showWithGravity(response.data.message , Toast.LONG, Toast.TOP);
                    navigation.navigate('NotifikasiAplikasi',{
                        id:id,
                        nama_aplikasi:nama_aplikasi,
                    });
                }else{
                    Toast.showWithGravity(response.data.message, Toast.LONG, Toast.TOP);
                }
                console.log(response.data);
            }catch(error){
                console.log(error.message);
            }
        };
        
        add();
    };
    
    const select = () => {

        const onSelectedItemsChange = (selectedItems) => {
            setselectedItems(selectedItems);
            
        }
        return (
            <View style={{ flex: 1 }}>
              <MultiSelect
                items={pengguna}
                uniqueKey="id_pengguna"
                onSelectedItemsChange={onSelectedItemsChange}
                selectedItems={selectedItems}
                selectText="Pick Penerima"
                searchInputPlaceholderText="Search Items..."
                // onChangeInput={ (text)=> console.warn(text)}
                tagRemoveIconColor="#CCC"
                tagBorderColor="#CCC"
                tagTextColor="#CCC"
                selectedItemTextColor="#CCC"
                selectedItemIconColor="#CCC"
                itemTextColor="#000"
                displayKey="nama_pengguna"
                searchInputStyle={{ color: '#CCC' }}
                submitButtonColor="#CCC"
                submitButtonText="Submit"
              />
            </View>
          );
    };

    const select2 = () => {

        const onSelectedItemsChangeMedia = (media) => {
            setMedia(media);
            
        }
        return (
            <View style={{ flex: 1 }}>
              <MultiSelect
                items={datamedia}
                uniqueKey="id_media"
                onSelectedItemsChange={onSelectedItemsChangeMedia}
                selectedItems={media}
                selectText="Pick Media"
                searchInputPlaceholderText="Search Items..."
                // onChangeInput={ (text)=> console.warn(text)}
                tagRemoveIconColor="#CCC"
                tagBorderColor="#CCC"
                tagTextColor="#CCC"
                selectedItemTextColor="#CCC"
                selectedItemIconColor="#CCC"
                itemTextColor="#000"
                displayKey="nama_media"
                searchInputStyle={{ color: '#CCC' }}
                submitButtonColor="#CCC"
                submitButtonText="Submit"
              />
            </View>
          );
    };
    
    return (
        <SafeAreaView style={{paddingHorizontal:20,flex:1,backgroundColor:COLORS.white}}>
            
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{marginTop: 70}}>
                    <Text style={{fontSize:27, fontWeight: "bold",color: COLORS.dark}}>Tambah Data</Text>
                </View>
                <View style={{ borderBottomWidth: 3, borderBottomColor: "black", marginTop:10,marginBottom:20 }} />
                <View>
                    <Text style={{ fontSize:18, fontWeight: "bold",color: COLORS.dark,marginBottom:5, marginTop:20 }}>Pilih Penerima</Text>
                    {select()}
                    <Text style={{ fontSize:18, fontWeight: "bold",color: COLORS.dark,marginBottom:5, marginTop:20 }}>Pilih Penerima</Text>
                    {select2()}
                </View>
                <View style={{ marginBottom:100 }}>
                    <View>
                        
                        <Text style={{ fontSize:18, fontWeight: "bold",color: COLORS.dark,marginBottom:5, marginTop:20 }}>Judul</Text>
                        <TextInput
                            placeholder="Nama" 
                            style={STYLES.input}
                            value={judul}
                            onChangeText={value => setJudul(value)}
                        />
                        <Text style={{ fontSize:18, fontWeight: "bold",color: COLORS.dark,marginBottom:5, marginTop:20 }}>Isi Notifikasi</Text>
                        <TextInput
                            placeholder="Isi Notifikasi" 
                            style={STYLES.input}
                            value={isi}
                            onChangeText={value => setIsi(value)}
                        />
                        
                        <View style={{ marginTop:100 }}>
                            
                            <Button
                            color={COLORS.back}
                            title="Kembali"
                            onPress={ function () {
                                navigation.navigate('NotifikasiAplikasi',{
                                    id:id,
                                    nama_aplikasi:nama_aplikasi,
                                });
                            }}
                            
                            />

                            <Button
                            color={COLORS.success}
                            title="Simpan"
                            onPress={  () => {
                                tambahData();
                            }}
                            
                            />
                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

export default NotifikasiAdd;