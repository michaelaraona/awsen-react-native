import React from 'react';

import {Image, StyleSheet, Text, View} from 'react-native';
import img from '../../assets/img/gambar3.jpg';
import COLORS from '../../consts/color';
import MapView,{PROVIDER_GOOGLE} from 'react-native-maps';
const Maps = () => {
    return (
        <View style={userStyles.halaman}>
          <View style={userStyles.gambar}>
            <View style={userStyles.wrap}>
              <Image source={img} style={userStyles.avatar} />
            </View>
            <Text style={userStyles.nama}>Michael Araona Wily</Text>
            <Text style={userStyles.npm}>06.2018.1.07072</Text>
          </View>

          <View style={userStyles.map}>
            <MapView
                style={StyleSheet.absoluteFillObject}
                provider={PROVIDER_GOOGLE}
                initialRegion={{
                    latitude: -7.526005,
                    longitude: 108.869512,
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.0121,
                  }}>
            </MapView>
          </View>
        </View>

        
      );
}

export default Maps;

const userStyles = StyleSheet.create({
  
    halaman: {
      backgroundColor: COLORS.white,
      flex: 1,
    },
    gambar: {
      flex: 1,
    },
    wrap: {
      width: 100,
      height: 100,
      borderWidth: 1,
      borderColor: '#eee',
      borderRadius: 100 / 2,
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 18,
      marginLeft:10,
      marginTop:30,

    },
    avatar: {
      width: 100,
      height: 100,
      borderRadius: 100 / 2,
    },
    nama: {
      fontSize: 24,
      textAlign: 'center',
      color: COLORS.secondary,
      fontWeight: '600',
      marginTop:-90,
      marginLeft: 50,
    },
    npm: {
      fontSize: 18,
      textAlign: 'center',
      fontWeight: '400',
      color: COLORS.dark,
      marginTop: 4,
      marginLeft: 50,
    },
    map :{
        height: 650,
        width: 400,
        justifyContent: 'flex-end',
        alignItems: 'center',
    }
});