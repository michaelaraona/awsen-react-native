import * as React from 'react';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { View, Text, Image, Alert, ScrollView } from "react-native";
import { Card, Button } from "react-native-paper";
import axios from 'axios';
import COLORS from '../../consts/color';

const Dashboaard = ({navigation}) => {
    const [dataUser , setDataUser] = React.useState([]);

    React.useEffect(function() {
        (async function() {
          const userJson = await AsyncStorage.getItem("user");
          const user = JSON.parse(userJson);
    
          setDataUser(user);
        })();
      }, []);
    
    
    return(
        <ScrollView>
            <View style={{marginTop: 20, marginLeft:10}}>
                    <Text style={{fontSize:27, fontWeight: "bold",color: COLORS.dark}}>Manajemen Notifikasi</Text>
                </View>
                <View style={{ borderBottomWidth: 3, borderBottomColor: "black", marginTop:10,marginBottom:20 }} />
            <View style={{ flexDirection: "row"}}>
                <Card style={{ height: 100 , marginTop: 20, marginLeft:20}} onPress = {() => navigation.navigate('Aplikasi')}>
                    {/* <Card.Cover source={ require("../assets/profil.png") } /> */}
                    <Card.Content>
                        <Button style={{ marginBottom: -15, marginLeft: -20, marginRight: -20 }}>Aplikasi</Button>
                    </Card.Content>
                </Card>
                <Card style={{ height: 100 , marginTop: 20, marginLeft:20}} onPress = {() => navigation.navigate('ListAplikasi')}>
                    {/* <Card.Cover source={ require("../assets/profil.png") } /> */}
                    <Card.Content>
                        <Button style={{ marginBottom: -15, marginLeft: -20, marginRight: -20 }}>Pengguna Aplikasi</Button>
                    </Card.Content>
                </Card>
            </View>
            <View style={{ flexDirection: "row"}}>
                <Card style={{ height: 100 , marginTop: 20, marginLeft:20}} onPress = {() => navigation.navigate('ListAplikasiNotifikasi')}>
                    {/* <Card.Cover source={ require("../assets/profil.png") } /> */}
                    <Card.Content>
                        <Button style={{ marginBottom: -15, marginLeft: -20, marginRight: -20 }}>Notifikasi</Button>
                    </Card.Content>
                </Card>
            </View>
        </ScrollView>
    );
};

export default Dashboaard;