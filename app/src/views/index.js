import Login from './Login';
import User from './User';
import Maps from './Maps';
import Aplikasi from './Aplikasi';
import Dashboaard from './Dashboard';
import AplikasiAdd from './Aplikasi/aplikasi-add';
import AplikasiEdit from './Aplikasi/aplikasi-edit';
import ListAplikasi from './Pengguna';
import PenggunaAplikasi from './Pengguna/pengguna-aplikasi';
import PenggunaAdd from './Pengguna/pengguna-tambah';
import PenggunaEdit from './Pengguna/pengguna-edit';
import ListAplikasiNotifikasi from './Notifikasi';
import NotifikasiAplikasi from './Notifikasi/notifikasi-aplikasi';
import NotifikasiAdd from './Notifikasi/notifikasi-add';
import NotifikasiKirim from './Notifikasi/notifikasi-kirim';




export {
    User,
    Login,
    Maps,
    Aplikasi,
    Dashboaard,
    AplikasiAdd,
    AplikasiEdit,
    ListAplikasi,
    PenggunaAplikasi,
    PenggunaAdd,
    PenggunaEdit,
    ListAplikasiNotifikasi,
    NotifikasiAplikasi,
    NotifikasiAdd,
    NotifikasiKirim,

};