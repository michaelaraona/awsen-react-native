import * as React from "react";
import {Text,  View , Button} from 'react-native';
import axios from 'axios';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import Toast from 'react-native-simple-toast';
import { ipGlobal } from "../../Global";
import AsyncStorageLib from "@react-native-async-storage/async-storage";
import COLORS from "../../consts/color";

const renderItem = function({item},navigation) {
    return(
        <TouchableOpacity
            onPress={ function() {
                navigation.navigate("AplikasiEdit", {
                    id: item.id_aplikasi
                });
                
            }}
        >
            <View style={{ flexDirection: "row", flexWrap: "wrap" , marginBottom:10}}>
                <View style={{ marginBottom: 5 }}>
                <Text style={{ fontSize: 16 ,color:'black'}}>Nama Aplikasi: {item.nama_aplikasi}</Text>
                </View>
            </View>

        </TouchableOpacity>
    );
};

const Aplikasi = function({navigation}) {
    const [aplikasi, setAplikasi] = React.useState([]);
    const [dataUser , setDataUser] = React.useState([]);
    
    React.useEffect(function() {
        (async function() {
          const userJson = await AsyncStorageLib.getItem("user");
          const user = JSON.parse(userJson);
    
          setDataUser(user);
        })();
      }, []);
    
    
    React.useEffect(() =>{
        const getData = async function() {
            let id_user = dataUser.id_user;
            try{
                const response = await axios.get(`http://${ipGlobal}/ManajemenNotifikasi/AplikasiApi/index/${id_user}`);
                setAplikasi(response.data.allApp);
                console.log(aplikasi);
            }catch(error){
                console.log(error);
            }
        };

        const timer = setTimeout(() => {
            getData();
          }, 7000);
        

        
        return () => clearTimeout(timer);
    }, [aplikasi]);
    return (
      <View style={{ marginBottom: 300 }}>
        
            
        <View style={{marginTop: 20, marginLeft:10}}>
                <Text style={{fontSize:27, fontWeight: "bold",color: COLORS.dark}}>Data Aplikasi</Text>
            </View>
        <Text style={{ marginVertical: 15, fontSize: 16, fontWeight: "bold", textAlign: "center" }}>Klik Data Untuk Opsi Update Delete</Text>
        <View style={{ flexDirection: "row", justifyContent: "space-evenly" }}>
            <Button
            color={COLORS.success}
            title="Tambah Data"
            onPress={()=> navigation.navigate('AplikasiAdd')}
            
            />
            <Button
            color={COLORS.back}
            title="Home"
            onPress={()=> navigation.navigate('Dashboard')}
            
            />
        </View>
        <View style={{ borderBottomWidth: 3, borderBottomColor: "black", marginTop:10,marginBottom:20 }} />
        <FlatList
          style={{ backgroundColor: "white" }}
          data={aplikasi}
          renderItem={ item => renderItem(item, navigation) }
          keyExtractor={item => item.id_aplikasi}
          ItemSeparatorComponent={function() {
            return <View style={{ borderBottomWidth: 1, borderBottomColor: "black",marginBottom:5 }} />;
          }}
        />
      </View>
    );
  }

export default Aplikasi;