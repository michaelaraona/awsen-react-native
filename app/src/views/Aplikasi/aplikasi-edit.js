import * as React from 'react';

import {
    View,
    Text , 
    TextInput,
    Button,
    TouchableOpacity,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import { SafeAreaView } from 'react-native-safe-area-context';
import COLORS from '../../consts/color';
import STYLES from '../../styles';
import { ipGlobal } from '../../Global';

const AplikasiEdit = ({route,navigation}) => {
    const [nama, setNama] = React.useState('');
    const[aplikasi,setAplikasi] = React.useState([]);
    const { id } = route.params;
    React.useEffect(function() {(async function() {
            try{
                const response = await axios.get(`http://${ipGlobal}/ManajemenNotifikasi/AplikasiApi/show/${id}`);
                setAplikasi(response.data.aplikasi);
                setNama(response.data.aplikasi.nama_aplikasi);
            }catch(error){
                console.log(error.message);
            }
        })();
      }, []);

    const update = () => {
        const update = async () => {
            try{
                const formData = new FormData();
                formData.append('aplikasi',nama);
                const response = await axios.post(`http://${ipGlobal}/ManajemenNotifikasi/AplikasiApi/editAplikasi/${id}`,formData);
                console.log(response.data);
                if(response.data.status == 200){
                    Toast.showWithGravity(response.data.message , Toast.LONG, Toast.TOP);
                    navigation.navigate('Aplikasi');
                }else{
                    Toast.showWithGravity(response.data.message, Toast.LONG, Toast.TOP);
                }
            }catch(error){
                console.log(error.message);
            }
        };
        
        update();
    };
    
    let row;
    if(aplikasi.status == 1){
        row = (
            <View style={{ flexDirection: "row", justifyContent: "space-evenly",marginTop:25 }}>
                <Button
                color={COLORS.danger}
                title="Non-Aktifkan"
                onPress={() => {
                    /* 1. Navigate to the Details route with params */
                    const nonAktifkan = async () => {
                        const response = await axios.get(`http://${ipGlobal}/ManajemenNotifikasi/AplikasiApi/nonAktifkan/${id}`);
                        Toast.showWithGravity(response.data.message , Toast.LONG, Toast.TOP);
                    };

                    nonAktifkan();
                    navigation.navigate('Aplikasi');
                }}
                
                /> 
            </View>                
            );
    }else{
        row = (<View style={{ flexDirection: "row", justifyContent: "space-evenly",marginTop:25 }}>
            <Button
            color={COLORS.secondary}
            title="Aktifkan"
            onPress={() => {
                /* 1. Navigate to the Details route with params */
                const aktifkan = async () => {
                    const response = await axios.get(`http://${ipGlobal}/ManajemenNotifikasi/AplikasiApi/aktifkan/${id}`);
                    Toast.showWithGravity(response.data.message , Toast.LONG, Toast.TOP);
                };

                aktifkan();
                navigation.navigate('Aplikasi');
            }}
            
            /> 
        </View>)
    }
    return (
        <SafeAreaView style={{paddingHorizontal:20,flex:1,backgroundColor:COLORS.white}}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{marginTop: 70}}>
                    <Text style={{fontSize:27, fontWeight: "bold",color: COLORS.dark}}>Manage Data Aplikasi</Text>
                </View>
                <View style={{ borderBottomWidth: 3, borderBottomColor: "black", marginTop:10,marginBottom:20 }} />
                <View>
                    <View>
                        
                        <Text style={{ fontSize:18, fontWeight: "bold",color: COLORS.dark,marginBottom:5, marginTop:20 }}>Nama Aplikasi</Text>
                        
                        <TextInput
                            placeholder="Nama Aplikasi" 
                            style={STYLES.input}
                            value={nama}
                            onChangeText={value => setNama(value)}
                        />
                        <View style={{ flexDirection: "row", justifyContent: "space-evenly",marginTop:25 }}>
                            <Button
                            color={COLORS.success}
                            title="Simpan"
                            onPress={update}
                            
                            />
                            <Button
                            color={COLORS.danger}
                            title="Hapus"
                            onPress={() => {
                                /* 1. Navigate to the Details route with params */
                                const deleteData = async () => {
                                    const response = await axios.get(`http://${ipGlobal}/ManajemenNotifikasi/AplikasiApi/deleteAplikasi/${id}`);
                                    Toast.showWithGravity(response.data.message , Toast.LONG, Toast.TOP);
                                };
            
                                deleteData();
                                navigation.navigate('Aplikasi');
                            }}
                            
                            />
                            <Button
                            color={COLORS.back}
                            title="Kembali"
                            onPress={ function () {
                                navigation.navigate('Aplikasi');
                            }}
                            
                            />
                        </View>

                        {row}
                        
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

export default AplikasiEdit;