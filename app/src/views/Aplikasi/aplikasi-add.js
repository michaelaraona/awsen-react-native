import React,{
    useState
} from 'react';
import {
    View,
    Text , 
    TextInput,
    Button,
    TouchableOpacity,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import  Icon  from 'react-native-vector-icons/MaterialIcons';
import COLORS from '../../consts/color';
import STYLES from '../../styles';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import { ipGlobal } from '../../Global';


const AplikasiAdd = ({navigation}) => {
    const [nama, setNama] = useState('');
    const [dataUser , setDataUser] = React.useState([]);

    React.useEffect(function() {
        (async function() {
          const userJson = await AsyncStorageLib.getItem("user");
          const user = JSON.parse(userJson);
    
          setDataUser(user);
        })();
      }, []);
    
    const tambahData = () => {
        if(nama == ''){
            Toast.show("Tolong Isi Semua Data !");
        }else{
            const add = async () => {
                let id_user = dataUser.id_user;
                try{
                    const formData = new FormData();
                    formData.append('id_user',id_user);
                    formData.append('nama',nama);
                    const response = await axios.post(`http://${ipGlobal}/ManajemenNotifikasi/AplikasiApi/addAplikasi`,formData);
                    if(response.data.status == 200){
                        Toast.showWithGravity(response.data.message , Toast.LONG, Toast.TOP);
                        navigation.navigate('Aplikasi');
                    }else{
                        Toast.showWithGravity(response.data.message, Toast.LONG, Toast.TOP);
                    }
                }catch(error){
                    console.log(error.message);
                }
            };
            
            add();
        }
    };
    
    return (
        <SafeAreaView style={{paddingHorizontal:20,flex:1,backgroundColor:COLORS.white}}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{marginTop: 70}}>
                    <Text style={{fontSize:27, fontWeight: "bold",color: COLORS.dark}}>Tambah Data</Text>
                </View>
                <View style={{ borderBottomWidth: 3, borderBottomColor: "black", marginTop:10,marginBottom:20 }} />
                <View>
                    <View>
                        
                        <Text style={{ fontSize:18, fontWeight: "bold",color: COLORS.dark,marginBottom:5, marginTop:20 }}>Nama Aplikasi</Text>
                        <TextInput
                            placeholder="Nama" 
                            style={STYLES.input}
                            value={nama}
                            onChangeText={value => setNama(value)}
                        />
                        
                        <View style={{ marginTop:30 }}>
                            <Button
                            color={COLORS.success}
                            title="Simpan"
                            onPress={tambahData}
                            
                            />
                            <Button
                            color={COLORS.back}
                            title="Kembali"
                            onPress={ function () {
                                navigation.navigate('Aplikasi');
                            }}
                            
                            />
                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
        
    );
}

export default AplikasiAdd;