import * as React from 'react';
import {
    View,
    Text , 
    TextInput,
    Button,
    TouchableOpacity,
} from 'react-native';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import { SafeAreaView } from 'react-native-safe-area-context';
import COLORS from '../../consts/color';
import STYLES from '../../styles';
import { ipGlobal } from '../../Global';

const renderItem = function({item},navigation,nama_aplikasi) {
    return(
        <TouchableOpacity
            onPress={ function() {
                navigation.navigate("PenggunaEdit", {
                    id: item.id_pengguna,
                    nama_aplikasi:nama_aplikasi,
                });
                
            }}
        >
            <View style={{ flexDirection: "row", flexWrap: "wrap"  ,marginBottom:10}}>
                <View style={{ marginBottom: 5 }}>
                    <Text style={{ fontSize: 13,color:'black' }}>Nama Pengguna: {item.nama_pengguna}</Text>
                    <Text style={{ fontSize: 13,color:'black' }}>Email : {item.email_pengguna} | No. Telp : {item.notelp_pengguna}</Text>
                    <Text style={{ fontSize: 13,color:'black' }}>Username Telegram : {item.username_telegram} </Text>
                </View>
            </View>

        </TouchableOpacity>
    );
};

const PenggunaAplikasi = ({route,navigation}) => {
    const [pengguna,setPengguna] = React.useState([]);
    const {id,nama_aplikasi} = route.params;

    React.useEffect(() =>{
        const getData = async function() {
            try{
                const response = await axios.get(`http://${ipGlobal}/ManajemenNotifikasi/PenggunaAplikasiApi/getPengguna/${id}`);
                setPengguna(response.data.pengguna);
            }catch(error){
                console.log(error);
            }
        };

        const timer = setTimeout(() => {
            getData();
            }, 5000);
        

        
        return () => clearTimeout(timer);
    }, [pengguna]);

    console.log(pengguna);

    return (
        <View style={{ marginBottom: 500}}>
            
            <View style={{flexDirection: "row", justifyContent: "space-evenly",marginTop: 20, marginLeft:10}}>
                <Text style={{fontSize:20, fontWeight: "bold",color: COLORS.dark}}>Data Pengguna Aplikasi ({nama_aplikasi})</Text>
            </View>
            <Text style={{ marginVertical: 15, fontSize: 16, fontWeight: "bold", textAlign: "center" }}>Klik Data Pengguna Untuk Opsi Update & Delete</Text>
            <View style={{ flexDirection: "row", justifyContent: "space-evenly" }}>
                <Button
                color={COLORS.success}
                title="Tambah Data"
                onPress={()=> navigation.navigate('PenggunaAdd',{
                    id:id,
                    nama_aplikasi:nama_aplikasi,
                })}
                
                />
                <Button
                color={COLORS.back}
                title="Kembali"
                onPress={()=> navigation.navigate('ListAplikasi')}
                
                />
            </View>
            <View style={{ borderBottomWidth: 3, borderBottomColor: "black", marginTop:10,marginBottom:20 }} />
            <View style={{ flexDirection: "row"}}>
            <FlatList
                style={{ backgroundColor: "white"}}
                data={pengguna}
                renderItem={ item => renderItem(item, navigation,nama_aplikasi) }
                keyExtractor={item => item.id_pengguna}
                ItemSeparatorComponent={function() {
                return <View style={{ borderBottomWidth: 1, borderBottomColor: "black",marginBottom:5 }} />;
                }}
            /> 
            </View>
           
        </View>
    );

};


export default PenggunaAplikasi;
