import * as React  from 'react';
import {
    View,
    Text , 
    TextInput,
    Button,
    TouchableOpacity,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import  Icon  from 'react-native-vector-icons/MaterialIcons';
import COLORS from '../../consts/color';
import STYLES from '../../styles';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import { ipGlobal } from '../../Global';


const PenggunaEdit = ({route,navigation}) => {
    const [nama, setNama] = React.useState('');
    const [telegram,setTelegram] = React.useState('');
    const [email,setEmail] = React.useState('');
    const [nomor,setNomor] = React.useState('');

    const {id,nama_aplikasi} = route.params;

    const [dataUser , setDataUser] = React.useState([]);
    const [pengguna, setPengguna] = React.useState([]);

    React.useEffect(function() {
        (async function() {
          const userJson = await AsyncStorageLib.getItem("user");
          const user = JSON.parse(userJson);
    
          setDataUser(user);
        })();
      }, []);
      
      React.useEffect(function() {(async function() {
        try{
            const response = await axios.get(`http://${ipGlobal}/ManajemenNotifikasi/PenggunaAplikasiApi/show/${id}`);
            setPengguna(response.data.pengguna);
            setNama(response.data.pengguna.nama_pengguna);
            setEmail(response.data.pengguna.email_pengguna);
            setNomor(response.data.pengguna.notelp_pengguna);
            setTelegram(response.data.pengguna.username_telegram);
        }catch(error){
            console.log(error.message);
        }
    })();
  }, []);
    
    const editData = () => {
        if(nama == '' || telegram == '' || email == '' || nomor == ''){
            Toast.show("Tolong Isi Semua Data !");
        }else{
            const edit = async () => {
                try{
                    const formData = new FormData();
                    formData.append('nomor',nomor);
                    formData.append('nama',nama);
                    formData.append('email',email);
                    formData.append('telegram',telegram);
                    const response = await axios.post(`http://${ipGlobal}/ManajemenNotifikasi/PenggunaAplikasiApi/editPengguna/${id}`,formData);
                    if(response.data.status == 200){
                        Toast.showWithGravity(response.data.message , Toast.LONG, Toast.TOP);
                        navigation.navigate('PenggunaAplikasi',{
                            id:pengguna.aplikasi_id,
                            nama_aplikasi:nama_aplikasi,
                        });
                    }else{
                        Toast.showWithGravity(response.data.message, Toast.LONG, Toast.TOP);
                    }
                }catch(error){
                    console.log(error.message);
                }
            };
            
            edit();
        }
    };
    
    return (
        <SafeAreaView style={{paddingHorizontal:20,flex:1,backgroundColor:COLORS.white}}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{marginTop: 70}}>
                    <Text style={{fontSize:27, fontWeight: "bold",color: COLORS.dark}}>Edit Data</Text>
                </View>
                <View style={{ borderBottomWidth: 3, borderBottomColor: "black", marginTop:10,marginBottom:20 }} />
                <View>
                    <View>
                        
                        <Text style={{ fontSize:18, fontWeight: "bold",color: COLORS.dark,marginBottom:5, marginTop:20 }}>Nama Pengguna</Text>
                        <TextInput
                            placeholder="Nama" 
                            style={STYLES.input}
                            value={nama}
                            onChangeText={value => setNama(value)}
                        />
                        <Text style={{ fontSize:18, fontWeight: "bold",color: COLORS.dark,marginBottom:5, marginTop:20 }}>Username Telegram</Text>
                        <TextInput
                            placeholder="Username Telegram" 
                            style={STYLES.input}
                            value={telegram}
                            onChangeText={value => setTelegram(value)}
                        />

                        <Text style={{ fontSize:18, fontWeight: "bold",color: COLORS.dark,marginBottom:5, marginTop:20 }}>Email</Text>
                        <TextInput
                            placeholder="Email" 
                            style={STYLES.input}
                            value={email}
                            onChangeText={value => setEmail(value)}
                        />

                        <Text style={{ fontSize:18, fontWeight: "bold",color: COLORS.dark,marginBottom:5, marginTop:20 }}>Nomor Telepon</Text>
                        <TextInput
                            placeholder="Nomor" 
                            style={STYLES.input}
                            value={nomor}
                            onChangeText={value => setNomor(value)}
                        />
                        
                        <View style={{ flexDirection: "row", justifyContent: "space-evenly",marginTop:25 }}>
                            <Button
                            color={COLORS.success}
                            title="Simpan"
                            onPress={editData}
                            
                            />

                            <Button
                            color={COLORS.danger}
                            title="Hapus"
                            onPress={() => {
                                /* 1. Navigate to the Details route with params */
                                const deleteData = async () => {
                                    const response = await axios.get(`http://${ipGlobal}/ManajemenNotifikasi/PenggunaAplikasiApi/deletePengguna/${pengguna.id_pengguna}`);
                                    Toast.showWithGravity(response.data.message , Toast.LONG, Toast.TOP);
                                };
            
                                deleteData();
                                navigation.navigate('PenggunaAplikasi',{
                                    id:pengguna.aplikasi_id,
                                    nama_aplikasi:nama_aplikasi,
                                });
                            }}
                            
                            />
                            <Button
                            color={COLORS.back}
                            title="Kembali"
                            onPress={ function () {
                                navigation.navigate('PenggunaAplikasi',{
                                    id:pengguna.aplikasi_id,
                                    nama_aplikasi:nama_aplikasi,
                                });
                            }}
                            
                            />
                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
        
    );
}

export default PenggunaEdit;