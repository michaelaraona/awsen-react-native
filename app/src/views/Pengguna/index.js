import * as React from 'react';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { View, Text, Image, Alert, ScrollView , Button} from "react-native";
import { Card } from "react-native-paper";
import axios from 'axios';
import COLORS from '../../consts/color';
import { ipGlobal } from '../../Global';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';

const renderItem = function({item},navigation) {
    return(
        <TouchableOpacity
            onPress={ function() {
                navigation.navigate("PenggunaAplikasi", {
                    id: item.id_aplikasi,
                    nama_aplikasi: item.nama_aplikasi,
                });
                
            }}
        >
            <View style={{ flexDirection: "row", flexWrap: "wrap"  ,marginBottom:10}}>
                <View style={{ marginBottom: 5 }}>
                <Text style={{ fontSize: 16,color:'black' }}>Nama Aplikasi: {item.nama_aplikasi}</Text>
                </View>
            </View>

        </TouchableOpacity>
    );
};

const ListAplikasi = ({navigation}) => {
    const [dataUser , setDataUser] = React.useState([]);
    const [aplikasi,setAplikasi] = React.useState([]);

    React.useEffect(function() {
        (async function() {
          const userJson = await AsyncStorage.getItem("user");
          const user = JSON.parse(userJson);
    
          setDataUser(user);
        })();
      }, []);
      React.useEffect(() =>{
        const getData = async function() {
            let id_user = dataUser.id_user;
            try{
                const response = await axios.get(`http://${ipGlobal}/ManajemenNotifikasi/PenggunaAplikasiApi/index/${id_user}`);
                setAplikasi(response.data.aplikasi);
            }catch(error){
                console.log(error);
            }
        };

        const timer = setTimeout(() => {
            getData();
          }, 5000);
        

        
        return () => clearTimeout(timer);
    }, [aplikasi]);
    return(
        <View style={{ marginBottom: 500}}>
            
            <View style={{marginTop: 20, marginLeft:10}}>
                <Text style={{fontSize:27, fontWeight: "bold",color: COLORS.dark}}>Pengguna(Data Aplikasi)</Text>
            </View>
            <Text style={{ marginVertical: 15, fontSize: 16, fontWeight: "bold", textAlign: "center" }}>Klik Data Aplikasi Untuk Opsi Pengguna Aplikasi</Text>
            <View style={{ flexDirection: "row", justifyContent: "space-evenly" }}>
                <Button
                color={COLORS.back}
                title="Home"
                onPress={()=> navigation.navigate('Dashboard')}
                
                />
            </View>
            <View style={{ borderBottomWidth: 3, borderBottomColor: "black", marginTop:10,marginBottom:20 }} />
            <View style={{ flexDirection: "row"}}>
            <FlatList
                style={{ backgroundColor: "white"}}
                data={aplikasi}
                renderItem={ item => renderItem(item, navigation) }
                keyExtractor={item => item.id_aplikasi}
                ItemSeparatorComponent={function() {
                return <View style={{ borderBottomWidth: 1, borderBottomColor: "black",marginBottom:5 }} />;
                }}
            />
            </View>
        </View>
    );
};

export default ListAplikasi;