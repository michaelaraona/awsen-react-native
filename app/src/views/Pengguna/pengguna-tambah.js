import * as React  from 'react';
import {
    View,
    Text , 
    TextInput,
    Button,
    TouchableOpacity,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import  Icon  from 'react-native-vector-icons/MaterialIcons';
import COLORS from '../../consts/color';
import STYLES from '../../styles';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import { ipGlobal } from '../../Global';


const PenggunaAdd = ({route,navigation}) => {
    const [nama, setNama] = React.useState('');
    const [telegram,setTelegram] = React.useState('');
    const [email,setEmail] = React.useState('');
    const [nomor,setNomor] = React.useState('');

    const {id,nama_aplikasi} = route.params;

    const [dataUser , setDataUser] = React.useState([]);

    React.useEffect(function() {
        (async function() {
          const userJson = await AsyncStorageLib.getItem("user");
          const user = JSON.parse(userJson);
    
          setDataUser(user);
        })();
      }, []);
    
    const tambahData = () => {
        if(nama == '' || telegram == '' || email == '' || nomor == ''){
            Toast.show("Tolong Isi Semua Data !");
        }else{
            const add = async () => {
                try{
                    const formData = new FormData();
                    formData.append('nomor',nomor);
                    formData.append('nama',nama);
                    formData.append('email',email);
                    formData.append('telegram',telegram);
                    const response = await axios.post(`http://${ipGlobal}/ManajemenNotifikasi/PenggunaAplikasiApi/storePengguna/${id}`,formData);
                    if(response.data.status == 200){
                        Toast.showWithGravity(response.data.message , Toast.LONG, Toast.TOP);
                        navigation.navigate('PenggunaAplikasi',{
                            id:id,
                            nama_aplikasi:nama_aplikasi,
                        });
                    }else{
                        Toast.showWithGravity(response.data.message, Toast.LONG, Toast.TOP);
                    }
                }catch(error){
                    console.log(error.message);
                }
            };
            
            add();
        }
    };
    
    return (
        <SafeAreaView style={{paddingHorizontal:20,flex:1,backgroundColor:COLORS.white}}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{marginTop: 70}}>
                    <Text style={{fontSize:27, fontWeight: "bold",color: COLORS.dark}}>Tambah Data</Text>
                </View>
                <View style={{ borderBottomWidth: 3, borderBottomColor: "black", marginTop:10,marginBottom:20 }} />
                <View>
                    <View>
                        
                        <Text style={{ fontSize:18, fontWeight: "bold",color: COLORS.dark,marginBottom:5, marginTop:20 }}>Nama Pengguna</Text>
                        <TextInput
                            placeholder="Nama" 
                            style={STYLES.input}
                            value={nama}
                            onChangeText={value => setNama(value)}
                        />
                        <Text style={{ fontSize:18, fontWeight: "bold",color: COLORS.dark,marginBottom:5, marginTop:20 }}>Username Telegram</Text>
                        <TextInput
                            placeholder="Username Telegram" 
                            style={STYLES.input}
                            value={telegram}
                            onChangeText={value => setTelegram(value)}
                        />

                        <Text style={{ fontSize:18, fontWeight: "bold",color: COLORS.dark,marginBottom:5, marginTop:20 }}>Email</Text>
                        <TextInput
                            placeholder="Email" 
                            style={STYLES.input}
                            value={email}
                            onChangeText={value => setEmail(value)}
                        />

                        <Text style={{ fontSize:18, fontWeight: "bold",color: COLORS.dark,marginBottom:5, marginTop:20 }}>Nomor Telepon</Text>
                        <TextInput
                            placeholder="Nomor" 
                            style={STYLES.input}
                            value={nomor}
                            onChangeText={value => setNomor(value)}
                        />
                        
                        <View style={{ marginTop:30 }}>
                            <Button
                            color={COLORS.success}
                            title="Simpan"
                            onPress={tambahData}
                            
                            />
                            <Button
                            color={COLORS.back}
                            title="Kembali"
                            onPress={ function () {
                                navigation.navigate('ListAplikasi');
                            }}
                            
                            />
                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
        
    );
}

export default PenggunaAdd;