const COLORS = {
    dark: "#000",
    light: "#a5a5a5",
    white: "#fff",
    primary: "#28388f",
    secondary: "#64beff",
    pink: "#ff2d5f",
    success : '#5cb85c',
    danger : '#d9534f',
    warning : '#f0ad4e',
    info : '#5bc0de',
    back : '#6c757d'
};

export default COLORS;