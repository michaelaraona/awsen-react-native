import { StyleSheet } from "react-native";
import COLORS from "../consts/color";


const STYLES = StyleSheet.create({
    inputContainer:{flexDirection: 'row', marginTop: 20},
    inputIcon:{
        marginTop: 15,
        position: "absolute",
    },
    input:{
        color: COLORS.light,
        paddingLeft : 30,
        borderBottomWidth: 0.5,
        flex : 1,
        fontSize: 18,
    },
    btnPrimary:{
        backgroundColor: COLORS.primary,
        height: 50,
        marginTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },
    halaman: {
        backgroundColor: COLORS.white,
        flex: 1,
      },
      gambar: {
        flex: 1,
      },
      wrap: {
        width: 100,
        height: 100,
        borderWidth: 1,
        borderColor: '#eee',
        borderRadius: 100 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 18,
        marginLeft:10,
        marginTop:30,

      },
      avatar: {
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
      },
      nama: {
        fontSize: 24,
        textAlign: 'center',
        color: COLORS.secondary,
        fontWeight: '600',
        marginTop:-90,
        marginLeft: 50,
      },
      npm: {
        fontSize: 18,
        textAlign: 'center',
        fontWeight: '400',
        color: COLORS.dark,
        marginTop: 4,
        marginLeft: 50,
      },
});

export default STYLES;